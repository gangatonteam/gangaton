<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;

class NavbarCategoriesComposer
{
    protected $categories;

    public function __construct()
    {
        $this->buildCategories();
    }

    public function buildCategories(){
        $this->categories = [
            [
                "Belleza",
                "Bienestar",
                "Gastronomía",
            ],
            [
                "Salud",
                "Servicios",
                "Tickets",
            ],
            [
                "Tiempo libre"
            ],

        ];
    }

    public function compose(View $view){
        $view->with('categories', $this->categories);
    }
}
