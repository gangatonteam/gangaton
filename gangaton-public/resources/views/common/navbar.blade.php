<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #4829b2">
    <div class="container">

        <a class="navbar-brand" href="index.html">
            GANGATON
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation"><span class="burger"><span></span></span></button>

        <div class="navbar-collapse collapse" id="navbarSupportedContent" style="">
            <ul class="navbar-nav align-items-center mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.html" role="button">
                        Home
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pages
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="landing-pages.html">
                            <span>Landing Pages</span>
                            <p>Start with one of pre-made templates.</p>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="inner-pages.html">
                            <span>Inner Pages</span>
                            <p>Extend your website with these pages.</p>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="app-pages.html">
                            <span>App Pages</span>
                            <p>Add functionality to your website.</p>
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown dropdown-mega">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown-1" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Components
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown-1">
                        <div class="row">

                            @foreach($categories as $category)
                                <div class="col">
                                    <ul class="mega-list">
                                        @foreach($category as $name)
                                            @php
                                                //class="highlight" usar esto cuando sea la categoria seleccionada
                                            @endphp
                                            <li><a href="#">{{$name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </li>
            </ul>

            <ul class="navbar-nav align-items-center mr-0">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#">Sign Up</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#">Sign in</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
