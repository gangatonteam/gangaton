
# Gangaton  
  
## Prerequisitos  
* PHP 7.1.3 o superior  
* [Composer](https://getcomposer.org/download/)
* [NodeJS](https://nodejs.org/es/)
* El cliente Git  
  
## Environment setup 

Para instalar el ambiente de desarrollo es necesario correr los siguientes scripts:
  
 * `git clone https://bitbucket.org/gangatonteam/gangaton/` 
 * `composer install`
 * `npm install` 
  
## Correr el proyecto  
Existen varias maneras de correr el proyecto
  
  
 1. Utilizando el servidor que viene por defecto con la versión nueva de
    PHP por medio del comando `php artisan serve` 
 2. Usando algun servidor de
    tipo lamp, wamp o mamp que tenga las tecnologías necesarias y las
    versiones correctas

  
## Tecnologías frontend  
  
En el proyecto se estan usando el preprocesador de css **SASS** y se esta compilando con **mix** que es proveido por laravel https://laravel.com/docs/5.7/mix  
  
### Resources  
* https://laravel.com/  
* https://lumen.laravel.com/